# Beckman Base Theme 2016

**Warning: alpha release. Expect breaking changes.**

This package extends [Bootstrap 4](https://v4-alpha.getbootstrap.com/) to provide a consistent base theme for SCSS-based [Beckman Institute](https://beckman.illinois.edu) web projects.

## Installation

0. In root directory of your project, run `npm init`.
0. Run `npm install --save https://gitlab.engr.illinois.edu/beckmancomms/beckman-theme-base-2016.git`.
   * Optional: Specify a [tagged revision](https://gitlab.engr.illinois.edu/beckmancomms/beckman-theme-base-2016/tags). E.g., `.../beckman-theme-base-2016.git#1.0.1`.

## Configuration

### SASS

By default, the Beckman theme disables unused Bootstrap components.

Components can be imported explicitly and re-enabled by copying the `Main.scss` file into your assets directory. Update the asset paths, then uncomment components you want to import.  
```scss
@import "bootstrap/scss/responsive-embed;
```

### Asset Paths

By default, the Beckman theme assumes assets (images, scripts, etc.) will be installed in `/Content/`. If your project uses a different directory structure, you will need to update the asset directory variables in `custom-variables.scss`. 

0. Copy the `custom-variables.scss` file to your project's assets directory as `project-variables.scss` and customize base values.
0. Copy `Main.scss` to your project's assets directory and add your variables file _before_ `custom-variables.scss`.
0. Adjust original asset paths as needed.

### Extending Styles

Beckman's base styles can be extended by adding custom components.

0. Add custom stylesheets to your project's assets directory.
0. Copy `Main.scss` to your project's assets directory and import custom components.

## Building Stylesheets

Build your stylesheets using your favorite build tool. The Beckman Communications group uses [Gulp](http://gulpjs.com/).

0. Configure your task runner.
0. Add tasks to build css.
    * If using the unmodified theme, point to `node_modules/beckman-theme-base-2016/assets/scss/Main.scss`.
    * If using a modified or extended theme, point to your project's main SASS file.
0. You may need to include all scss paths in your build task:
    * Beckman assets: `node_modules/beckman-theme-base-2016/assets/scss/`
    * Bootstrap assets: `node_modules/beckman-theme-base-2016/node_modules/`
    * Your project's assets
0. Add tasks to copy scripts to production directories, which should match the `$script-directory` variable defined in `custom-variables.scss`.
0. Optional: Add tasks to copy sourcemaps and/or scss files to production directories.

### Sample gulp build task

```js
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var rename = require('gulp-rename');
var cssnano = require('gulp-cssnano');
var notify = require('gulp-notify');

var config = {
    // ...
}

gulp.task('compile-sass', function () {
    var projectStyles = 'Content/assets/scss/';
    var sassMain = projectStyles + 'Main.scss';
    var beckmanStyles = 'node_modules/beckman-theme-base-2016/assets/scss/';
    var bootstrapStyles = 'node_modules/';
    var options = {
        includePaths: [
            projectStyles,
            beckmanStyles,
            bootstrapStyles
        ]
    };
    return gulp.src(config.sassMain)
        .pipe(sourcemaps.init())
        .pipe(sass(options))
        .pipe(autoprefixer(config.autoprefixerOptions.browsers))
        .pipe(gulp.dest(config.cssDir))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(cssnano({}))
        .pipe(sourcemaps.write('.', { includeContent: false }))
        .pipe(gulp.dest(config.cssDir))
        .pipe(notify('Finished compiling SASS files.'))
        .on('error', sass.logError);
});

gulp.task('copy-assets', function() {
    var assetDirs = {
        "images": 'node_modules/beckman-theme-base-2016/assets/images/**/*'
    };

    for (var dir in assetDirs) {
        if (assetDirs.hasOwnProperty(dir)) {
            gulp.src(assetDirs[dir])
                .pipe(gulp.dest(config.dest + '/' + dir));
        }
    }
});

gulp.task('build', ['compile-sass', 'copy-assets']);
```

## HTML Samples

See [Bootstrap 4 documentation](https://v4-alpha.getbootstrap.com/getting-started/introduction/) for base styles.

See [bootstrap/mainline branch of Beckman.Website](https://gitlab.engr.illinois.edu/beckmancomms/beckman-website/tree/bootstrap/mainline) for Beckman pages and custom styles.

## Fallback Requirements

1. Uses CSS object-fit for header band images, which is not compatible with IE or Edge. Detect with `Modernizr.objectfit`.