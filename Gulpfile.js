var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');

var config = {
    imageSource: 'assets/images/**/*',
    imageDest: 'assets/images/'
};

gulp.task('minify-images', function () {
    return gulp.src(config.imageSource)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(config.imageDest));
});

gulp.task('build', ['minify-images'])