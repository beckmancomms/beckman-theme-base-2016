# Changelog

## 1.0.0-alpha3

* Save runtime dependencies (e.g., jquery, tether) as peer dependencies.
* Remove unused package dependencies.

## 1.0.0-alpha2

* Remove NCSA license until properly approved.

## 1.0.0-alpha1

* Create proof-of-concept package and documentation.